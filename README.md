# python generator

Basic odoo modules generator wizard.

This wizard started as a personal project to make easier to start new odoo projects setting the main fields and classes from same odoo web interface.

It will allow administrators to generate their forms fast and easy without coding unless it's necessary.

Starting odoo version: 11.0

classes prefix: pyg

Classes:
    Module (pyg_module)
    Model (pyg_model)
    Field (pyg_field)

**MODULE**

This class represents the module which is about to be generated. Defines the main settings for this one:
*  Project name
*  Base name (it means the real underscored directory name for module)
*  Description (Starting with simple text)
*  Dependencies (Required modules: comma separated)
*  Parent module (To do)

**MODEL**

Defines the module's models, setting the usual parameters for each
* Name (As a reference)
* Inherits (Flag of inheritance from existing model)
* Inherit model (Defines the model base name which inherits from)
* Base name (the model _name)
* Description (Reference)
* Module (project's module where it belongs)

**FIELD**

Defines the fields we will have on each model, fields can work with 
* Integer
* Float
* String
* Selectable
* Many2one
* One2many

