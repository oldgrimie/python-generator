# -*- coding: utf-8 -*-
{
    'name': 'Python module generator',
    'version': '1.0',
    'summary': 'Odoo module generator',
    'sequence': 40,
    'description': """
    Makes easier and faster the basic projects starting, tool target for administrators
    """,
    'category': 'Administration',
    'website': '',
    # 'images' : ['images/a_picture.jpeg'],
    'depends': ['base'],
    'data': [
        'views/pyg_module_tree.xml',
        'views/pyg_module_form.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
