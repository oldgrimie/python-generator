# -*- coding: utf-8 -*-
from odoo import models, fields, api
import os

class pyg_model(models.Model):
    _name = 'pyg.model'

    model_name = fields.Char('Name')
    model_inherits = fields.Boolean('Is Inherit', default=False)
    model_inherit_name = fields.Char(
        'Inherit model', help='Base Model that inherits from')
    model_base_name = fields.Char('Base name')
    model_description = fields.Char('Description')
    model_field_ids = fields.One2many('pyg.field', 'field_model_id', 'Fields')
    model_module_id = fields.Many2one('pyg.module', 'Module')
    rec_name = 'model_name'

    @api.onchange('model_name')
    def change_model_name(self):
        self.model_base_name = self.model_name.lower().replace(' ', '.')

