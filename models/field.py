# -*- coding: utf-8 -*-
from odoo import models, fields, api


class pyg_field(models.Model):
    _name = 'pyg.field'

    FIELD_TYPES = [
        ('Char', 'Char'),
        ('Text', 'Long Text'),
        ('Selection', 'Selection'),
        ('Integer', 'Integer'),
        ('Float', 'Float number'),
        ('Many2one', 'Many to One'),
        ('One2many', 'One to Many'),
        ('Many2many', 'Many to Many')
    ]

    field_name = fields.Char('Name')
    field_type = fields.Selection(FIELD_TYPES, 'Type')
    field_max = fields.Integer('Max.', default=-1)
    field_min = fields.Integer('Min.', default=-1)
    field_default = fields.Char('Default value')
    field_values = fields.Text(
        'Available values',
        help='Eg: (\'val1\', \'label1\'),(\'val2\', \'label2\')')
    field_model_id = fields.Many2one('pyg.model', 'Model', required=True)
    # For Related fields
    field_existingmodel = fields.Boolean('From existing module', default=False)
    field_relatedmodel_id = fields.Many2one('pyg.model', 'Related model')
    field_relatedmodel_name = fields.Char('Related Model')
    field_digits = fields.Char('Digits', help='Real (no rounding) digits')
    field_digits_display = fields.Char('Display digits',
                                       help='visible (rounding) digits')
    field_compute_method = fields.Boolean('Compute method', default=False)
    field_stored = fields.Boolean('Store computed', default=False)
    field_treeview = fields.Boolean('Form', default=False)
    field_formview = fields.Boolean('Tree', default=False)
    field_searchview = fields.Boolean('Search', default=False)
    field_priority = fields.Integer('Priority')

    _rec_name = 'field_name'
    _order = ' field_model_id, field_priority'
