# -*- coding: utf-8 -*-
from odoo import models, fields, api

class pyg_module(models.Model):

    _name = 'pyg.module'

    module_name = fields.Char('Name')
    module_base_name = fields.Char('Base name')
    module_description = fields.Text('Description')
    module_requires = fields.Char('Dependencies',
                                  help='comma separated: account,hr,sales')
    module_parent_module = fields.Char('Parent Module')
    module_model_ids = fields.One2many('pyg.model', 'mdl_mod_id', 'Models')
    _rec_name = 'module_name'

    @api.onchange('module_name')
    def change_module_name(self):
        self.module_base_name = self.module_name.lower().replace(' ', '_')
